#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QGraphicsSvgItem>
#include <QImage>
#include <QSvgRenderer>

MainWindow::MainWindow(QWidget *parent):
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QGraphicsScene *scene2 = new QGraphicsScene(this);
    QImage          image(scene2->width(), scene2->height(), QImage::Format_ARGB32);
    QSvgRenderer   *renderer = new QSvgRenderer(QString(":/sample.svg"));        // path of .svg file
    QPainter        painter(&image);
    renderer->render(&painter);
    QGraphicsSvgItem *f = new QGraphicsSvgItem();
    f->setSharedRenderer(renderer);

    scene2->setSceneRect(QRectF(0, 0, scene2->width(), scene2->height()));
    scene2->addItem(f);

    ui->view->setScene(scene2);
    ui->view->show();
}

MainWindow::~MainWindow()
{
    delete ui;
}
